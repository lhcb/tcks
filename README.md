# TCKs

This repository hosts the trigger configurations for Run 3. 

Every trigger configuration is a commit and a lightweight tag, which serves as the key.
The commits are made on branches, which group and provide a history for related configurations.
The branch names conicide with the (HLT) "type" of the TCK.

TCKs are created, validated and pushed here by adding specification files to [lhcb/tck-specs](https://gitlab.cern.ch/lhcb/tck-specs/).
The deployment happens automatically.

## TCK structure (TODO)
